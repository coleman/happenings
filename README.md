# happenings

An ion script to print redox project info.

Right now we are simply listing recently updated issues in redox and ion. It's
still a lot of output, so we should find a way to constrain it based on script
parameters.

### Requirements

* ion shell
* jq to parse json
* a gitlab personal access token exported to your shell env as `REDOX_GITLAB_TOKEN`

### Usage

Clone this project and symlink the script onto your PATH. Run without an arg

```
$ happenings
```

Or provide a redox project name (see the code to see which are available)

```
$ happenings ion
```

![usage example](usage.png)

### TODO

* If no project name passed, maybe print fewer issues
* Explore other kinds of info: build pipelines, commit messages, etc
* Generate This Week in Redox markdown skeletons
* Smarter [pagination](https://docs.gitlab.com/ee/api/#pagination)
* Caching to be nicer to the server

